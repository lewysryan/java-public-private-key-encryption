package LewEncryption.lcrypt;

public class Testing {

    public static void main(String[] args) {
	EncryptionHandler e = new EncryptionHandler();
	byte[] message = null;
	try {
	    message = e.encrypt("Hello, this message is secret!".getBytes());
	    System.out.println(new String(message)); //Encrypted form
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	
	try {
	    String uncrypted = new String(e.decrypt(message));
	    System.out.println(uncrypted); //Decrypted back again
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}