package LewEncryption.lcrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

/**
 * ---With help from---
 * http://snipplr.com/view/18368/ 
 * http://www.informit.com/articles/article.aspx?p=170967&seqNum=4
 */
public class EncryptionHandler {

    public final static String ENCRYPTION_ALGORITHM = "RSA";

    private KeyFactory keyFactory;

    public PublicKey publicKey;
    public PrivateKey privateKey;

    private Cipher cipher;

    public EncryptionHandler() {
	try {
	    this.cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
	    this.keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
	    this.setupKeys();	    
	} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
	    e.printStackTrace();
	}
    }

    private void setupKeys() {
	File folder;

	try {
	    folder = new File(Testing.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());

	    folder.mkdirs();

	    try {
		KeyPair pair = loadKeyPair(folder.getAbsolutePath());
		this.publicKey = pair.getPublic();
		this.privateKey = pair.getPrivate();
	    } catch (Exception e) {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(ENCRYPTION_ALGORITHM);
		kpg.initialize(1024); // 1024 is the keysize.
		KeyPair kp = kpg.generateKeyPair();

		this.publicKey = kp.getPublic();
		this.privateKey = kp.getPrivate();

		saveKeyPair(folder.getAbsolutePath(), kp);
	    }
	} catch (URISyntaxException | IOException | NoSuchAlgorithmException e) {
	    e.printStackTrace();
	}
    }

    public byte[] encrypt(byte[] stringBytes) throws Exception {
	this.cipher.init(Cipher.ENCRYPT_MODE, publicKey);
	return cipher.doFinal(stringBytes);
    }

    public byte[] decrypt(byte[] stringBytes) throws Exception {
	this.cipher.init(Cipher.DECRYPT_MODE, privateKey);
	return cipher.doFinal(stringBytes);
    }

    public void saveKeyPair(String path, KeyPair keyPair) throws IOException {
	PrivateKey privateKey = keyPair.getPrivate();
	PublicKey publicKey = keyPair.getPublic();

	// Store Public Key.
	X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
	FileOutputStream fos = new FileOutputStream(path + "/public.key");
	fos.write(x509EncodedKeySpec.getEncoded());
	fos.close();

	// Store Private Key.
	PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
	fos = new FileOutputStream(path + "/private.key");
	fos.write(pkcs8EncodedKeySpec.getEncoded());
	fos.close();
    }

    public KeyPair loadKeyPair(String path) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
	// Read Public Key.
	File filePublicKey = new File(path + "/public.key");
	FileInputStream fis = new FileInputStream(path + "/public.key");
	byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
	fis.read(encodedPublicKey);
	fis.close();

	// Read Private Key.
	File filePrivateKey = new File(path + "/private.key");
	fis = new FileInputStream(path + "/private.key");
	byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
	fis.read(encodedPrivateKey);
	fis.close();

	// Generate KeyPair
	X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
	PublicKey publicKey = this.keyFactory.generatePublic(publicKeySpec);

	PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
	PrivateKey privateKey = this.keyFactory.generatePrivate(privateKeySpec);

	return new KeyPair(publicKey, privateKey);
    }

    public byte[] encodePublicKey() {
	X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
	return x509EncodedKeySpec.getEncoded();
    }

    public PublicKey decodePublicKey(byte[] key) throws InvalidKeySpecException, NoSuchAlgorithmException {
	X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(key);
	return this.keyFactory.generatePublic(publicKeySpec);
    }
}